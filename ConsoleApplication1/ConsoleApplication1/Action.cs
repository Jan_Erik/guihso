﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prototyp_kontext
{
    public class Action
    {
        Func<bool> func;

        public bool TriggerFunction()
        {
            return func();
        }
    }
}
