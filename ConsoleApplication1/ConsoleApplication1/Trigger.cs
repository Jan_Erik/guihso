﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prototyp_kontext
{
    public delegate void TriggerEvent();
    //public delegate void OnTriggerExit();

    public class Trigger
    {
        private bool isActive;

        public event TriggerEvent enter;
        public event TriggerEvent exit;

        public void OnTriggerEnter()
        {
            if(enter != null)
            {
                isActive = true;
                enter();
            }
        }

        public void OnTriggerExit()
        {
            if(exit != null)
            {
                isActive = false;
                exit();
            }
        }
    }
}
