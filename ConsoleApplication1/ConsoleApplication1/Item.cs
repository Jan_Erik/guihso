﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prototyp_kontext
{
    public class Item
    {
        private Item parent;
        private List<Item> children;
        private bool isActive;
        private Transform transform;
        private RenderObj renderObj;
        private List<Behaviour> behaviours;

        public Item()
        {
            this.parent = null;
            this.children = new List<Item>();
            this.isActive = false;
            this.transform = null;
            this.renderObj = null;
            this.behaviours = new List<Behaviour>();
        }

        public Item(Item parent, List<Item> children, Transform trans, RenderObj renderObj, List<Behaviour> behaviours)
        {
            this.parent = parent;
            this.children = children;
            this.transform = trans;
            this.renderObj = renderObj;
            this.behaviours = behaviours;
            this.isActive = false;
        }
        
        public void AddBehaviour(Behaviour behaviour)
        {
            this.behaviours.Add(behaviour);
        }

        public void RemoveBehaviour(Behaviour behaviour)
        {
            if (this.behaviours.Contains(behaviour))
            {
                this.behaviours.Remove(behaviour);
            }
        }

        public Item Parent
        {
            get
            {
                return parent;
            }

            set
            {
                parent = value;
            }
        }

        public List<Item> Children
        {
            get
            {
                return children;
            }

            set
            {
                children = value;
            }
        }

        public bool IsActive
        {
            get
            {
                return isActive;
            }

            set
            {
                isActive = value;
            }
        }

        public Transform Transform
        {
            get
            {
                return transform;
            }

            set
            {
                transform = value;
            }
        }

        public RenderObj RenderObj
        {
            get
            {
                return renderObj;
            }

            set
            {
                renderObj = value;
            }
        }

        public List<Behaviour> Behaviours
        {
            get
            {
                return behaviours;
            }

            set
            {
                behaviours = value;
            }
        }
    }
}
